import pymongo

from accounts import mongodb_endpoint


def search_news(query):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinitySearchEngine']
    col = db['news']
    results = col.aggregate([
        {
            '$search': {
                'text': {
                    'query': query,
                    'path': ['title', 'url']
                }
            }
        },
        {
            '$limit': 20
        },
        {
            '$project': {
                '_id': 1,
                'url': 1,
                'title': 1,
                'date': 1,
                'display_date': 1,
            }
        }
    ])

    return list(results)
