import requests
import pymongo
import html
from bs4 import BeautifulSoup
from random import choice
import time

from accounts import mongodb_endpoint

desktop_agents = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0']


def random_headers():
    return {'User-Agent': choice(desktop_agents),
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}


def get_formatted_string(text):
    text = text.replace('\n', ' ')
    text = text.replace('\t', ' ')
    text = text.split()
    text = ' '.join(text)
    return text


def format_xml_news(news_url):
    try:
        xml_page = requests.get(news_url, headers=random_headers()).text
        soup = BeautifulSoup(xml_page, 'xml')
        news_list = soup.findAll('url')

        news_articles = []
        for news in news_list:
            try:
                news = html.unescape(news)
                url = news.loc.text
                url = get_formatted_string(url)
                title = news.news.title.text
                title = get_formatted_string(title)
                date = news.publication_date.text
                date = get_formatted_string(date)
                display_date = date.split('T')[0]
                date_arr = display_date.split('-')
                display_date = date_arr[1] + '-' + date_arr[2] + '-' + date_arr[0]

                news_articles.append({'url': url, 'title': title, 'date': date, 'display_date': display_date})
            except Exception:
                continue

        return news_articles

    except Exception:
        return []


def get_reuters():
    return format_xml_news('https://www.reuters.com/sitemap_news_index1.xml') + format_xml_news(
        'https://www.reuters.com/sitemap_news_index2.xml')


def get_wsj():
    return format_xml_news('https://www.wsj.com/wsjsitemaps/wsj_google_news.xml')


def get_wiki_news():
    return format_xml_news(
        'https://en.wikinews.org/w/index.php?title=Special:NewsFeed&feed=sitemap&namespace=0&count=1000&hourcount=504&ordermethod=categoryadd')


def get_usa_today_news():
    return format_xml_news('https://www.usatoday.com/news-sitemap.xml')


def get_nyt_news():
    return format_xml_news('https://www.nytimes.com/sitemaps/new/news.xml.gz')


def update_news_in_mongo(news):
    client = pymongo.MongoClient(mongodb_endpoint)
    db = client['InfinitySearchEngine']
    col = db['news']
    col.delete_many({})
    col.insert_many(news)


def get_all_news():
    reuters = get_reuters()
    wsj = get_wsj()
    wiki_news = get_wiki_news()
    nyt = get_nyt_news()
    usa_today = get_usa_today_news()
    all_news = reuters + wsj + wiki_news + nyt + usa_today
    return all_news


def infinity_news():
    news = get_all_news()
    update_news_in_mongo(news)


def main_application():
    # Just updates the news every 10 minutes
    while True:
        try:
            infinity_news()
            time.sleep(600)
        except Exception:
            time.sleep(10)
            continue


if __name__ == '__main__':
    main_application()
