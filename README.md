# Infinity News
Infinity News is the code for how we find and index our news articles. 

## Requirements 
To run this program, you need Python3 and a MongoDB cloud atlas database. 

## Usage 
1. Update your mongoDB connection string in accounts.py
2. Run news.py to start populating the database (it updates every 10 minutes)
3. Create a search index on your "news" collection (From the "InfinitySearchEngine" database: https://docs.atlas.mongodb.com/reference/atlas-search/tutorial/#fts-tutorial-ref
4. After the index creation completes, you can now search 
the database with the search_results.py file by calling the search_news function


